﻿using RestSharp;
using System.Web;

namespace SimpleRokuConsoleApp
{

    /// <summary>
    /// Class responsible for sending requests to the Roku device (you can think of like your tv remote)
    /// </summary>
    class RokuController
    {
        private RokuDevice _device;

        /// <summary>
        /// Default constructor (private)
        /// </summary>
        private RokuController() { }
        
        /// <summary>
        /// Only accessable constructor. Object can not be created unless a device has been found and created. Use the DeviceManager class to find devices.
        /// </summary>
        /// <param name="initDevice">The device that you want to send requests to (at least initialy)</param>
        public RokuController(RokuDevice initDevice)
        {
            _device = new RokuDevice(initDevice);            
        }

        /// <summary>
        /// The device to send the requests to
        /// </summary>
        public RokuDevice Device { get => _device; set => _device = value; }

        /// <summary>
        /// Sends async requests to the device. Since the Roku device will not respond to the POST requests, waiting for the response is a waste of time so just fire and forget
        /// </summary>
        /// <param name="requestString">The request string. You can get a list possible request strings from the Roku API</param>
        private void SendRequest(string requestString)
        {
            var request = NetworkManager.Instance.CreateRequest(requestString);
            NetworkManager.Instance.SetUrl(_device.LocalAddress);
            request.Method = Method.POST;
            var response = NetworkManager.Instance.HttpClient.ExecuteTaskAsync(request);
        }


        #region KeyboardInputs

        /// <summary>
        /// Equivalent to pressing 'a' on the keyboard
        /// </summary>
        public void KeyPress_A()
        {
            SendRequest("keypress/Lit_a");
        }

        /// <summary>
        /// Equivalent to pressing 'b' on the keyboard
        /// </summary>
        public void KeyPress_B()
        {
            SendRequest("keypress/Lit_b");
        }

        /// <summary>
        /// Equivalent to pressing 'c' on the keyboard
        /// </summary>
        public void KeyPress_C()
        {
            SendRequest("keypress/Lit_c");
        }

        /// <summary>
        /// Equivalent to pressing 'd' on the keyboard
        /// </summary>
        public void KeyPress_D()
        {
            SendRequest("keypress/Lit_d");
        }

        /// <summary>
        /// Equivalent to pressing 'e' on the keyboard
        /// </summary>
        public void KeyPress_E()
        {
            SendRequest("keypress/Lit_e");
        }

        /// <summary>
        /// Equivalent to pressing 'f' on the keyboard
        /// </summary>
        public void KeyPress_F()
        {
            SendRequest("keypress/Lit_f");
        }

        /// <summary>
        /// Equivalent to pressing 'g' on the keyboard
        /// </summary>
        public void KeyPress_G()
        {
            SendRequest("keypress/Lit_g");
        }

        /// <summary>
        /// Equivalent to pressing 'h' on the keyboard
        /// </summary>
        public void KeyPress_H()
        {
            SendRequest("keypress/Lit_h");
        }

        /// <summary>
        /// Equivalent to pressing 'i' on the keyboard
        /// </summary>
        public void KeyPress_I()
        {
            SendRequest("keypress/Lit_i");
        }

        /// <summary>
        /// Equivalent to pressing 'j' on the keyboard
        /// </summary>
        public void KeyPress_J()
        {
            SendRequest("keypress/Lit_j");
        }

        /// <summary>
        /// Equivalent to pressing 'k' on the keyboard
        /// </summary>
        public void KeyPress_K()
        {
            SendRequest("keypress/Lit_k");
        }

        /// <summary>
        /// Equivalent to pressing 'l' on the keyboard
        /// </summary>
        public void KeyPress_L()
        {
            SendRequest("keypress/Lit_l");
        }

        /// <summary>
        /// Equivalent to pressing 'm' on the keyboard
        /// </summary>
        public void KeyPress_M()
        {
            SendRequest("keypress/Lit_m");
        }

        /// <summary>
        /// Equivalent to pressing 'n' on the keyboard
        /// </summary>
        public void KeyPress_N()
        {
            SendRequest("keypress/Lit_n");
        }

        /// <summary>
        /// Equivalent to pressing 'o' on the keyboard
        /// </summary>
        public void KeyPress_O()
        {
            SendRequest("keypress/Lit_o");
        }

        /// <summary>
        /// Equivalent to pressing 'p' on the keyboard
        /// </summary>
        public void KeyPress_P()
        {
            SendRequest("keypress/Lit_p");
        }

        /// <summary>
        /// Equivalent to pressing 'q' on the keyboard
        /// </summary>
        public void KeyPress_Q()
        {
            SendRequest("keypress/Lit_q");
        }

        /// <summary>
        /// Equivalent to pressing 'r' on the keyboard
        /// </summary>
        public void KeyPress_R()
        {
            SendRequest("keypress/Lit_r");
        }

        /// <summary>
        /// Equivalent to pressing 's' on the keyboard
        /// </summary>
        public void KeyPress_S()
        {
            SendRequest("keypress/Lit_s");
        }

        /// <summary>
        /// Equivalent to pressing 't' on the keyboard
        /// </summary>
        public void KeyPress_T()
        {
            SendRequest("keypress/Lit_t");
        }

        /// <summary>
        /// Equivalent to pressing 'u' on the keyboard
        /// </summary>
        public void KeyPress_U()
        {
            SendRequest("keypress/Lit_u");
        }

        /// <summary>
        /// Equivalent to pressing 'v' on the keyboard
        /// </summary>
        public void KeyPress_V()
        {
            SendRequest("keypress/Lit_v");
        }

        /// <summary>
        /// Equivalent to pressing 'w' on the keyboard
        /// </summary>
        public void KeyPress_W()
        {
            SendRequest("keypress/Lit_w");
        }

        /// <summary>
        /// Equivalent to pressing 'x' on the keyboard
        /// </summary>
        public void KeyPress_X()
        {
            SendRequest("keypress/Lit_x");
        }

        /// <summary>
        /// Equivalent to pressing 'y' on the keyboard
        /// </summary>
        public void KeyPress_Y()
        {
            SendRequest("keypress/Lit_y");
        }

        /// <summary>
        /// Equivalent to pressing 'z' on the keyboard
        /// </summary>
        public void KeyPress_Z()
        {
            SendRequest("keypress/Lit_z");
        }

        /// <summary>
        /// Equivalent to pressing 'enter' on the keyboard
        /// </summary>
        public void KeyPress_Enter()
        {
            SendRequest("keypress/Enter");
        }

        /// <summary>
        /// Equivalent to pressing 'space' on the keyboard
        /// </summary>
        public void KeyPress_Space()
        {
            string a = HttpUtility.UrlEncode(" ").Replace("+", "%20");
            SendRequest("keypress/Lit_" + a);
        }

        /// <summary>
        /// Equivalent to pressing 'back space' on the keyboard
        /// </summary>
        public void KeyPress_Backspace()
        {
            SendRequest("keypress/Backspace");
        }

        #endregion

        #region MenuInputs

        /// <summary>
        /// Equivalent to pressing 'power' on the Roku Remote
        /// </summary>
        public void Power()
        {           
            SendRequest("keypress/Power");
        }

        /// <summary>
        /// Equivalent to pressing 'home' on the Roku Remote
        /// </summary>
        public void Home()
        {
            SendRequest("keypress/Home");
        }

        /// <summary>
        /// Equivalent to pressing 'search' on the Roku Remote.
        /// This feature doesn't seem to ever work for me. The TV makes a ding so I kknow it's receiving the request; however, nothing happens. 
        /// It could be that my TV doesn't support this feature.
        /// </summary>
        public void Search()
        {
            SendRequest("keypress/Search");
        }


        /// <summary>
        /// Equivalent to pressing 'mute' on the Roku Remote
        /// </summary>
        public void Mute()
        {
            SendRequest("keypress/VolumeMute");
        }


        /// <summary>
        /// Equivalent to pressing 'volume up' on the Roku Remote
        /// </summary>
        public void VolumeUp()
        {
            SendRequest("keypress/VolumeUp");
        }


        /// <summary>
        /// Equivalent to pressing 'volume down' on the Roku Remote
        /// </summary>
        public void VolumeDown()
        {
            SendRequest("keypress/VolumeDown");
        }


        /// <summary>
        /// Equivalent to pressing 'left arrow' on the Roku Remote
        /// </summary>
        public void LeftArrow()
        {           
            SendRequest("keypress/Left");
        }

        /// <summary>
        /// Equivalent to pressing 'right arrow' on the Roku Remote
        /// </summary>
        public void RightArrow()
        {  
            SendRequest("keypress/Right");
        }

        /// <summary>
        /// Equivalent to pressing 'up arrow' on the Roku Remote
        /// </summary>
        public void UpArrow()
        {          
            SendRequest("keypress/Up");
        }

        /// <summary>
        /// Equivalent to pressing 'down arrow' on the Roku Remote
        /// </summary>
        public void DownArrow()
        {            
            SendRequest("keypress/Down");
        }

        /// <summary>
        /// Equivalent to pressing 'rewind' on the Roku Remote
        /// </summary>
        public void Reverse()
        {
            SendRequest("keypress/Rev");
        }

        /// <summary>
        /// Equivalent to pressing 'fast forward' on the Roku Remote
        /// </summary>
        public void Forward()
        {
            SendRequest("keypress/Fwd");
        }

        /// <summary>
        /// Equivalent to pressing 'back' on the Roku Remote
        /// </summary>
        public void Back()
        {
            SendRequest("keypress/Back");
        }

        /// <summary>
        /// Equivalent to pressing 'info (*)' on the Roku Remote
        /// </summary>
        public void Info()
        {
            SendRequest("keypress/Info");
        }

        /// <summary>
        /// Rewinds ten seconds
        /// </summary>
        public void InstantReplay()
        {
            SendRequest("keypress/InstantReplay");
        }


        /// <summary>
        /// Equivalent to pressing 'play' on the Roku Remote
        /// </summary>
        public void Play()
        {
            SendRequest("keypress/Play");
        }


        /// <summary>
        /// Equivalent to pressing 'select' or 'OK' on the Roku Remote
        /// </summary>
        public void Select()
        {           
            SendRequest("keypress/Select");
        }

        #endregion

        /// <summary>
        /// Launches the app with the associated ID. Will only launch the app if it is already installed. 
        /// It will not install the app. You can get the IDs of the installed apps by accessing the apps member of the roku device object that is
        /// currently selected. Be sure to execute RokuDevice::PopulateApps first though.
        /// </summary>
        /// <param name="appID">ID of the app you want to launch.</param>
        public void LaunchApp(int appID)
        {
            SendRequest("launch/" + appID);
        }
        
    }
}

﻿using RestSharp;
using System;

namespace SimpleRokuConsoleApp
{
    /// <summary>
    /// Thread safe singleton class used to mange the http client provided by the RestSharp library. 
    /// </summary>
    class NetworkManager
    {
        private static NetworkManager _instance = null;
        private static readonly object _padlock = new object();
        private RestClient _httpClient;

        /// <summary>
        /// Default and only constructor. It is not accessable outside of class because this class is 
        /// a singleton.
        /// </summary>
        private NetworkManager()
        {
            _httpClient = new RestClient();
        }

        /// <summary>
        /// Returns the only instance of this class. It is thread safe.
        /// </summary>
        public static NetworkManager Instance
        {
            get
            {
                lock (_padlock)
                {
                    if (_instance == null)
                    {
                        _instance = new NetworkManager();
                    }
                    return _instance;
                }
            }
        }

        /// <summary>
        /// The RestSharp http client being managed by this class
        /// </summary>
        public RestClient HttpClient { get => _httpClient; set => _httpClient = value; }

        /// <summary>
        /// Returns a created RestRequest object formed by the string provided.
        /// </summary>
        /// <param name="requestString">request string </param>
        /// <returns></returns>
        public RestRequest CreateRequest(string requestString)
        {
            var request = new RestRequest(requestString);
            return request;
        }

        /// <summary>
        /// Sets the location that requests should be sent to
        /// </summary>
        /// <param name="url">url string</param>
        public void SetUrl(string url)
        {
            _httpClient.BaseUrl = new Uri(url);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Net;
using System.Xml.Linq;

namespace SimpleRokuConsoleApp
{

    /// <summary>
    /// Class that represents a single Roku device found on the network
    /// </summary>
    class RokuDevice
    {
        private readonly string _localAddress;
        private readonly string _name;
        private Dictionary<int, RokuApp> _apps;
        private bool _populateAppsSuccess;
        private int _appCount;

        /// <summary>
        /// Default constructor (private)
        /// </summary>
        private RokuDevice() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="initLocalAddress">The local network address of the device</param>
        /// <param name="initName">The friendly name of the device</param>
        public RokuDevice(string initLocalAddress, string initName)
        {
            _appCount = 0;
            _localAddress = initLocalAddress;
            _name = initName;
            _apps = new Dictionary<int, RokuApp>();
            _populateAppsSuccess = false;
        }

        /// <summary>
        /// Copy constructor
        /// </summary>
        /// <param name="init_device">Device that you wish to copy</param>
        public RokuDevice(RokuDevice init_device)
        {
            _appCount = 0;
            _localAddress = init_device.LocalAddress;
            _name = init_device.Name;
            _apps = new Dictionary<int, RokuApp>();
            _populateAppsSuccess = false;
        }

        /// <summary>
        /// Queries the Roku Device (the actual device, NOT this object) for the apps installed and populates the _apps field
        /// </summary>
        /// <returns></returns>
        public async Task PopulateApps()
        {
            _appCount = 0;
            _populateAppsSuccess = false;
            _apps.Clear();

            var request = NetworkManager.Instance.CreateRequest("query/apps");
            NetworkManager.Instance.SetUrl(LocalAddress);
            var response = await NetworkManager.Instance.HttpClient.ExecuteTaskAsync(request);

            if (response.StatusCode == HttpStatusCode.OK)
            {               
                var xdoc = XDocument.Parse(response.Content);

                foreach (var i in xdoc.Descendants("app"))
                {
                    if (!Int32.TryParse(i.Attribute("id").Value, out int numValue))
                    {
                        continue;
                    }

                    _appCount++;
                    var app = new RokuApp(numValue, i.Value);
                    _apps.Add(_appCount, app);                  
                }

                _populateAppsSuccess = true;
            }
            
        }        

        /// <summary>
        /// The local network address of the device
        /// </summary>
        public string LocalAddress { get => _localAddress; }

        /// <summary>
        /// The friendly name of the device
        /// </summary>
        public string Name { get => _name; }

        /// <summary>
        /// Flag that can be checked to ensure that the _apps field has been populated
        /// </summary>
        public bool PopulateAppsSuccess { get => _populateAppsSuccess; }

        /// <summary>
        /// The Roku apps installed on the device
        /// </summary>
        public Dictionary<int, RokuApp> Apps { get => _apps; }

        /// <summary>
        /// The number of installed apps on the Roku device
        /// </summary>
        public int AppCount { get => _appCount; }
    }
}

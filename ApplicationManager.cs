﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SimpleRokuConsoleApp
{

    /// <summary>
    /// Oversees all aspects of this application including the UI/UX, ecxecution of user input by utilizing the various managment classes, controlling the main program loop and exiting of said loop, ect...
    /// </summary>
    class ApplicationManager
    {
        private RokuDeviceManager _deviceManager;
        private RokuController _rokuController;
        private bool _programShouldRun;
        private bool _keyboardEnabled;

        /// <summary>
        /// Default and only constructor
        /// </summary>
        public ApplicationManager()
        {
            _deviceManager = new RokuDeviceManager();
            _programShouldRun = true;
            _keyboardEnabled = false;
        }
        
        /// <summary>
        /// Outputs welcome message to the console.
        /// </summary>
        static void PrintWelcome()
        {
            Console.WriteLine("Welcome to my simple roku device controller console app!");
            System.Threading.Thread.Sleep(500);
        }

        /// <summary>
        /// Prints a loading screen displaying the message provided until the task provided is completed.
        /// The 'loading' component is simply dots appearing and dissapearing to the right of the message.
        /// </summary>
        /// <param name="message">Message to display</param>
        /// <param name="t">The task that you are waiting on</param>
        void PrintLoadingScreen(string message, Task t)
        {
            Console.Clear();

            string loadingstring = message;
            int c = 0;

            while (!t.IsCompleted)
            {
                for (int i = 0; i < c; i++)
                {
                    loadingstring += ".";
                }

                Console.WriteLine(loadingstring);
                System.Threading.Thread.Sleep(250);
                Console.SetCursorPosition(0, Console.CursorTop - 1);
                ClearCurrentConsoleLine();
                c++;

                if (c % 4 == 0)
                {
                    c = 0;
                }

                loadingstring = message;
            }
        }

        /// <summary>
        /// Searches for compatible devices and prints a message telling the user that. 
        /// </summary>
        void SearchForDevices()
        {
           
            var t = _deviceManager.SearchForDevices();

            PrintLoadingScreen("Please wait while compatible devices are found", t);

            if (_deviceManager.DeviceCount > 0)
            {
                Console.Clear();
                Console.WriteLine(_deviceManager.DeviceCount.ToString() + " device(s) found!");

                if (_deviceManager.DeviceCount == 1)
                {
                    Console.WriteLine("Since only one device was found, it has been automatically selected for you.");
                    System.Threading.Thread.Sleep(1000);
                    EnterTVRemoteLoop();
                }
                else
                {
                    PrintDeviceSelectionMenu();

                }
            }
            else
            {
                Console.WriteLine("No compatible devices found. Would you like to search again? (enter 'y' or 'n')");
                if (Console.ReadKey().Key == ConsoleKey.Y)
                {
                    Console.Clear();
                    SearchForDevices();
                }
                else
                {
                    return;
                }
            }
        }

        /// <summary>
        /// Attempts to populate the apps installed on the roku device
        /// </summary>
        void PopulateApps()
        {
            if (_deviceManager.SelectedDevice == null)
            {
                RestartAfterError();
            }

            if (_rokuController == null)
            {
                _rokuController = new RokuController(_deviceManager.SelectedDevice);
            }

            var t = _deviceManager.SelectedDevice.PopulateApps();
            PrintLoadingScreen("Populating Roku apps menu", t);

            if (_deviceManager.SelectedDevice.PopulateAppsSuccess == true && _deviceManager.SelectedDevice.AppCount > 0)
            {
                PrintAppsMenu();
            }
            else
            {
                Console.WriteLine("Sorry but no apps have been found.");
                System.Threading.Thread.Sleep(1000);
            }
        }

        /// <summary>
        /// Prints out a list of the installed apps to the console
        /// </summary>
        void PrintAppsMenu()
        {           
            Console.WriteLine("-----------------------------------------------------------------------------------------------------");

            foreach (var i in _deviceManager.SelectedDevice.Apps)
            {
                Console.WriteLine(i.Key + ":  " + i.Value.Name);
            }

            Console.WriteLine("-----------------------------------------------------------------------------------------------------");
            Console.Write("Please enter the number corrosponding to the app you would like to launch and press enter: ");
            WaitForAppsMenuInput();
        }

        /// <summary>
        /// Starts the application
        /// </summary>
        public void Start()
        {
            PrintWelcome();
            SearchForDevices();
        }

        /// <summary>
        /// Prints the menu for the user to select a device to send the requests to. This is UNTESTED because I only have one Roku device at home.
        /// </summary>
        void PrintDeviceSelectionMenu()
        {
            Console.WriteLine();

            foreach (KeyValuePair<int, RokuDevice> kvp in _deviceManager.Devices)
            {
                Console.WriteLine("{0}. {1}", kvp.Key, kvp.Value.Name);
            }

            Console.WriteLine();
            Console.Write("Please select a device by entering its corresponding number and press enter: ");
            WaitForDeviceSelection();
        }

        /// <summary>
        /// Waits for the users selection of the device they want to use
        /// </summary>
        void WaitForDeviceSelection()
        {
            string selection = Console.ReadLine();
            if (Int32.TryParse(selection, out int numValue))
            {
                if (_deviceManager.TrySetDeviceByKey(numValue))
                {
                    if (_deviceManager.SelectedDevice != null)
                    {
                        Console.Write(_deviceManager.SelectedDevice.Name + " Selected!");
                        System.Threading.Thread.Sleep(500);
                    }
                    else
                    {
                        RestartAfterError();
                    }

                }
                else
                {
                    Console.Clear();
                    Console.WriteLine("Your selection was not valid. Please try again.");
                    PrintDeviceSelectionMenu();
                }
            }
            else
            {
                Console.Clear();
                Console.WriteLine("Your selection was not valid. Please try again.");
                PrintDeviceSelectionMenu();
            }

        }

        /// <summary>
        /// Waits for the users selection of the app that they want to launch
        /// </summary>
        void WaitForAppsMenuInput()
        {
            var selection = Console.ReadLine();
            if (Int32.TryParse(selection, out int numValue))
            {
                if(_deviceManager.SelectedDevice.Apps.TryGetValue(numValue, out RokuApp app))
                {
                    _rokuController.LaunchApp(app.Id);
                }
                else
                {
                    Console.WriteLine("Sorry the app id you entered does not match any installed app on the device. Going back to main menu...");
                    System.Threading.Thread.Sleep(1500);
                }
            }
            else
            {
                Console.WriteLine("Selection was not a valid number Going back to main menu...");
                System.Threading.Thread.Sleep(1500);
            }
        }

        /// <summary>
        /// Enters the primary remote loop that waits for the users selection
        /// </summary>
        private void EnterTVRemoteLoop()
        {

            if (_deviceManager.SelectedDevice == null)
            {
                RestartAfterError();
            }

            _rokuController = new RokuController(_deviceManager.SelectedDevice);

            while (_programShouldRun)
            {
                DisplayTVRemoteMenu();
            }
        }

        /// <summary>
        /// Prints out the main menu interface to the console
        /// </summary>
        public void DisplayTVRemoteMenu()
        {
            Console.Clear();

            Console.WriteLine("-----------------------------------------------------------------------------------------------------");
            Console.WriteLine("Commands:");
            Console.WriteLine("'p' to power on/off tv");
            Console.WriteLine("'a' to select an app from menu");
            Console.WriteLine("'f10' to rewind");
            Console.WriteLine("'f9' to play");
            Console.WriteLine("'f12' to fast forward");
            Console.WriteLine("'backkspace' to go back");
            Console.WriteLine("'i' to get info");
            Console.WriteLine("'r' to activate instant replay");
            Console.WriteLine("'h' to go to the roku home screeen");
            Console.WriteLine("'m' to mute TV");
            Console.WriteLine("'page-up' to increase volume");
            Console.WriteLine("'page-down' to decrease volume");
            Console.WriteLine("'up key' to move up");
            Console.WriteLine("'down key' to move down");
            Console.WriteLine("'left key' to move left");
            Console.WriteLine("'right key' to move right");           
            Console.WriteLine("'f' to search");
            Console.WriteLine("'enter' to select");
            Console.WriteLine("'e' to Exit This Application");
            Console.WriteLine("------------------------------------------------------------------------------------------------------");

            WaitForTVRemoteInput();
        }

        /// <summary>
        /// Prints out the keyboard interface menu to the console
        /// </summary>
        public void DisplayKeyboardInputMenu()
        {
            Console.Clear();

            Console.WriteLine("------------------------------------------------------------------------------------------------------");
            Console.WriteLine("Keyboard Enabled! Type what you want and press enter (pressing enter may not actually do anything). 'Escape' to exit keyboard.");         
            Console.WriteLine("------------------------------------------------------------------------------------------------------");

            WaitForKeyboardInput();
        }

        /// <summary>
        /// Waits for the user to enter a menu selection and executes the selection
        /// </summary>
        public void WaitForTVRemoteInput()
        {
            if (_rokuController.Device == null)
            {
                RestartAfterError();
            }

            var key = Console.ReadKey().Key;

            switch (key)
            {

                case ConsoleKey.P:
                    _rokuController.Power();
                    break;

                case ConsoleKey.H:
                    _rokuController.Home();
                    break;
                   
                case ConsoleKey.E:
                    Exit();
                    break;

                case ConsoleKey.F:
                    _rokuController.Search();
                    break;

                case ConsoleKey.A:
                    PopulateApps();
                    break;

                case ConsoleKey.M:
                   _rokuController.Mute();
                   break;
                case ConsoleKey.PageUp:
                    _rokuController.VolumeUp();
                    break;
                case ConsoleKey.PageDown:
                    _rokuController.VolumeDown();
                    break;
                
               case ConsoleKey.K:
                   _keyboardEnabled = true;
                    EnterKeyboardLoop();
                   break;
               
                case ConsoleKey.RightArrow:
                   _rokuController.RightArrow();
                   break;
               case ConsoleKey.LeftArrow:
                    _rokuController.LeftArrow();
                   break;
               case ConsoleKey.UpArrow:
                    _rokuController.UpArrow();
                   break;
               case ConsoleKey.DownArrow:
                    _rokuController.DownArrow();
                   break;
               case ConsoleKey.Enter:
                    _rokuController.Select();
                   break;
                case ConsoleKey.F10:
                    _rokuController.Reverse();
                    break;
                case ConsoleKey.F9:
                    _rokuController.Play();
                    break;
                case ConsoleKey.F12:
                    _rokuController.Forward();
                    break;
                case ConsoleKey.R:
                    _rokuController.InstantReplay();
                    break;
                case ConsoleKey.I:
                    _rokuController.Info();
                    break;
                case ConsoleKey.Backspace:
                    _rokuController.Back();
                    break;
                /*
           case ConsoleKey.S:
               GetDeviceinfo();
               break;
           */
                default:                   
                    break;
            }
        }

        /// <summary>
        /// Enters the keyboard loop so that the user can type on the keyboard
        /// </summary>
        private void EnterKeyboardLoop()
        {
            if (_deviceManager.SelectedDevice == null)
            {
                RestartAfterError();
            }

            if (_rokuController == null)
            {
                _rokuController = new RokuController(_deviceManager.SelectedDevice);
            }

            while (_keyboardEnabled)
            {
                DisplayKeyboardInputMenu();
            }
        }

        /// <summary>
        /// Waits for the users input for on the keyboard
        /// </summary>
        private void WaitForKeyboardInput()
        {
            if (_rokuController.Device == null)
            {
                RestartAfterError();
            }

            var key = Console.ReadKey().Key;

            switch (key)
            {

                case ConsoleKey.A:
                    _rokuController.KeyPress_A();
                    break;

                case ConsoleKey.B:
                    _rokuController.KeyPress_B();
                    break;

                case ConsoleKey.C:
                    _rokuController.KeyPress_C();
                    break;

                case ConsoleKey.D:
                    _rokuController.KeyPress_D();
                    break;

                case ConsoleKey.E:
                    _rokuController.KeyPress_E();
                    break;

                case ConsoleKey.F:
                    _rokuController.KeyPress_F();
                    break;

                case ConsoleKey.G:
                    _rokuController.KeyPress_G();
                    break;

                case ConsoleKey.H:
                    _rokuController.KeyPress_H();
                    break;

                case ConsoleKey.I:
                    _rokuController.KeyPress_I();
                    break;

                case ConsoleKey.J:
                    _rokuController.KeyPress_J();
                    break;

                case ConsoleKey.K:
                    _rokuController.KeyPress_K();
                    break;

                case ConsoleKey.L:
                    _rokuController.KeyPress_L();
                    break;

                case ConsoleKey.M:
                    _rokuController.KeyPress_M();
                    break;

                case ConsoleKey.N:
                    _rokuController.KeyPress_N();
                    break;

                case ConsoleKey.O:
                    _rokuController.KeyPress_O();
                    break;

                case ConsoleKey.P:
                    _rokuController.KeyPress_P();
                    break;

                case ConsoleKey.Q:
                    _rokuController.KeyPress_Q();
                    break;

                case ConsoleKey.R:
                    _rokuController.KeyPress_R();
                    break;

                case ConsoleKey.S:
                    _rokuController.KeyPress_S();
                    break;

                case ConsoleKey.T:
                    _rokuController.KeyPress_T();
                    break;

                case ConsoleKey.U:
                    _rokuController.KeyPress_U();
                    break;

                case ConsoleKey.V:
                    _rokuController.KeyPress_V();
                    break;

                case ConsoleKey.W:
                    _rokuController.KeyPress_W();
                    break;

                case ConsoleKey.X:
                    _rokuController.KeyPress_X();
                    break;

                case ConsoleKey.Y:
                    _rokuController.KeyPress_Y();
                    break;

                case ConsoleKey.Z:
                    _rokuController.KeyPress_Z();
                    break;

                case ConsoleKey.Enter:
                    _rokuController.KeyPress_Enter();
                    break;

                case ConsoleKey.Backspace:
                    _rokuController.KeyPress_Backspace();
                    break;

                case ConsoleKey.Spacebar:
                    _rokuController.KeyPress_Space();
                    break;

                case ConsoleKey.Escape:
                    _keyboardEnabled = false;
                    break;

                default:
                    break;
            }
        }

        /// <summary>
        /// Clears the console line at the cursor position
        /// </summary>
        private void ClearCurrentConsoleLine()
        {
            int currentLineCursor = Console.CursorTop;
            Console.SetCursorPosition(0, Console.CursorTop);
            Console.Write(new string(' ', Console.WindowWidth));
            Console.SetCursorPosition(0, currentLineCursor);
        }

        /// <summary>
        /// Restarts the application after an error
        /// </summary>
        private void RestartAfterError()
        {
            Console.Clear();
            Console.WriteLine("An error has occurred. Restarting...");
            System.Threading.Thread.Sleep(1000);
            Restart();
        }

        /// <summary>
        /// Restarts the application
        /// </summary>
        private void Restart()
        {
            Console.Clear();
            _deviceManager.Restart();
            Start();
        }

        /// <summary>
        /// Sets the 'exit' flag so that the program loop will terminate
        /// </summary>
        private void Exit()
        {
            _programShouldRun = false;
        }
    }
}

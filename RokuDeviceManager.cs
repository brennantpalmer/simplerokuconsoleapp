﻿using Rssdp;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SimpleRokuConsoleApp
{
    /// <summary>
    /// Class designed to manage the finding and organizing of compatible Roku devices on the network
    /// </summary>
    class RokuDeviceManager
    {
        private Dictionary<int, RokuDevice> _devices;
        private RokuDevice _selectedDevice;
        private int _deviceCount;

        /// <summary>
        /// Sets all the field values to their defaults
        /// </summary>
        public RokuDeviceManager()
        {
            _deviceCount = 0;
            _devices = new Dictionary<int, RokuDevice>();
            _selectedDevice = null;
        }

        /// <summary>
        /// Total number of devices being managed.
        /// </summary>
        public int DeviceCount { get => _deviceCount; }

        /// <summary>
        /// The devices being managed.
        /// </summary>
        public Dictionary<int, RokuDevice> Devices { get => _devices; }

        /// <summary>
        /// The device that requests will be sent to.
        /// </summary>
        public RokuDevice SelectedDevice { get => _selectedDevice; set => _selectedDevice = value; }

        /// <summary>
        /// Will attempt to set the currently selected device by the key associated with it in the devices container
        /// </summary>
        /// <param name="key">The key of the device that you wish to select</param>
        /// <returns>Returns true if the device was found and selected and false otherwise</returns>
        public bool TrySetDeviceByKey(int key)
        {
            bool success = _devices.TryGetValue(key, out _selectedDevice);            
            return success;
        }

        /// <summary>
        /// Searches the network for Roku devices
        /// </summary>
        /// <returns></returns>
        public async Task SearchForDevices()
        {
            
            using (var deviceLocator = new SsdpDeviceLocator())
            {
                var foundDevices = await deviceLocator.SearchAsync("roku:ecp"); 

                foreach (var foundDevice in foundDevices)
                {                       
                    var fullDevice = await foundDevice.GetDeviceInfo(); 
                    var d = new RokuDevice(foundDevice.DescriptionLocation.ToString(), fullDevice.FriendlyName);
                    _deviceCount++;
                    _devices.Add(_deviceCount, d);
                }
            }

            if (_deviceCount == 1)
            {
                _devices.TryGetValue(1, out _selectedDevice);
            }
        }

        /// <summary>
        /// Resets all the fields to their default values
        /// </summary>
        public void Restart()
        {
            _deviceCount = 0;
            _devices.Clear();
            _selectedDevice = null;
        }
    }
}

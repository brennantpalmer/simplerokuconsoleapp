﻿
namespace SimpleRokuConsoleApp
{
    /// <summary>
    /// A class that represents an installed app on the Roku Device
    /// </summary>
    class RokuApp
    {
        private int _id;
        private string _name;

        /// <summary>
        /// ID of the app
        /// </summary>
        public int Id { get => _id; }

        /// <summary>
        /// Name of the app
        /// </summary>
        public string Name { get => _name; }

        /// <summary>
        /// Default constructor (private)
        /// </summary>
        private RokuApp(){ }

        /// <summary>
        /// Use to initialize RokuApp object. Field values cannot be modified after construction
        /// </summary>
        /// <param name="initID">ID of the app</param>
        /// <param name="initName">Name of the app</param>
        public RokuApp(int initID, string initName)
        {
            _id = initID;
            _name = initName;
        }
    }
}
